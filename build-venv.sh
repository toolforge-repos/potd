#!/bin/bash
set -euxo pipefail

cd ~/potd
python3 -m venv .venv

source .venv/bin/activate

pip install --upgrade pip setuptools wheel
pip install --upgrade -r requirements.txt
