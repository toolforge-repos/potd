#!/usr/bin/python3
# Initally written by https://commons.wikimedia.org/wiki/User:Pfctdayelise under Creative Commons Attribution-ShareAlike 3.0 license
# <https://creativecommons.org/licenses/by-sa/3.0/legalcode>
# Mostly re-written by https://www.mediawiki.org/wiki/User:Legoktm to
# use modern APIs and not wget.
import json
from email.message import EmailMessage
import re
import sys
import datetime
from pathlib import Path
import requests
import smtplib
from jinja2 import Template, Environment, FileSystemLoader
import mwparserfromhell


def template() -> Template:
    env = Environment(loader=FileSystemLoader("."), autoescape=True)
    return env.get_template("template.html")

def api(**kwargs):
    kwargs['formatversion'] = 2
    kwargs['format'] = 'json'
    r = requests.get('https://commons.wikimedia.org/w/api.php', params=kwargs)
    if not r.ok:
        r.raise_for_status()
    return r.json()


def page_content(title):
    params = {
        'action': 'query',
        'prop': 'revisions',
        'rvprop': 'content',
        'titles': title,
    }
    data = api(**params)
    return data['query']['pages'][0]['revisions'][0]['content']


def get_today_potd_title(today):
    d = today.strftime('%Y-%m-%d')
    return 'Template:Potd/' + d


def get_today_potd(title):
    content = page_content(title)
    code = mwparserfromhell.parse(content)
    name = str(code.filter_templates()[0].get(1).value)
    # Avoid bug when the comment is kept in the template
    # https://commons.wikimedia.org/w/index.php?title=Template%3APotd%2F2022-12-06&diff=717399978&oldid=700576888
    name = re.sub("<!--.*-->", "", name, 0, re.DOTALL).strip()
    return 'File:' + name


def file_url(title):
    return 'https://commons.wikimedia.org/wiki/' + title.replace(' ', '_')


def get_metadata(title):
    params = {
        'action': 'query',
        'prop': 'imageinfo',
        'iiprop': 'extmetadata|url',
        'iilimit': '10',
        'iiurlwidth': '600',
        'titles': title,
    }
    data = api(**params)['query']['pages'][0]['imageinfo'][0]
    return data['extmetadata'], data['thumburl']


def expand_templates(text):
    params = {
        'action': 'expandtemplates',
        'text': text
    }
    data = api(**params)
    return data['expandtemplates']['wikitext']


def get_language_name(lang):
    return expand_templates('{{#language:%s}}' % lang)


def get_captions(title) -> dict:
    params = {
        'action': 'query',
        'list': 'allpages',
        'apfrom': title.split(':', 1)[1],
        'aplimit': '100',
        'apnamespace': '10'
    }
    data = api(**params)
    langs = {}
    prefix = title + ' '
    for item in data['query']['allpages']:
        if item['title'].startswith(prefix):
            lang = item['title'].split('(')[1].split(')')[0]
            langs[lang] = item['title']
    ret = {}
    for lang in sorted(langs):
        lang_name = get_language_name(lang)
        content = page_content(langs[lang])
        if content.strip().startswith('#REDIRECT'):
            # ???
            continue
        code = mwparserfromhell.parse(content)
        try:
            temp = code.filter_templates()[0]
        except IndexError:
            continue
        caption_code = temp.get(1).value
        # We want templates like {{w|FooBar}} to render, so expand them
        expanded = expand_templates(str(caption_code))
        caption = str(mwparserfromhell.parse(expanded).strip_code())
        ret[lang_name] = caption

    return ret


mailfrom = 'Wikimedia Commons Picture of the Day <tools.potd@toolforge.org>'
mailto = "daily-image-l@lists.wikimedia.org"
if len(sys.argv) > 1:
        mailto = sys.argv[1]


def build_context():
    today = datetime.datetime.utcnow().date()
    potd_title = get_today_potd_title(today)
    file_title = get_today_potd(potd_title)
    metadata, thumb_url = get_metadata(file_title)

    ctx = {
        'image_url': file_url(file_title),
        'captions': get_captions(potd_title),
        'date': today.strftime('%B %-d, %Y'),
        'thumb_url': thumb_url,
        'subject': str(today),
    }
    if 'UsageTerms' in metadata:
        ctx['license'] = metadata['UsageTerms']['value']
    try:
        preview = ctx['captions']['English']
    except KeyError:
        preview = "Wikimedia Commons Picture of the day"
    ctx['preview'] = preview
    return ctx


def create_plaintext(ctx) -> str:
    text = "Picture of the day:\r\n"

    text += ctx['image_url'] + '\n'
    if 'license' in ctx:
        text += 'Copyright status: ' + ctx['license'] + '\n'
    text += 'Descriptions:\n'
    for lang, caption in ctx['captions'].items():
        text += f'{lang}: {caption}\n'
    print(text)
    return text


def main():
    ctx = build_context()
    msg = EmailMessage()
    msg['From'] = mailfrom
    msg['To'] = mailto
    msg['Subject'] = ctx['subject']
    plaintext = create_plaintext(ctx)
    html = template().render(ctx)
    msg.set_content(plaintext)
    msg.add_alternative(html, subtype="html")
    cfg = json.loads(Path("config.json").read_text())
    smtp = smtplib.SMTP_SSL if cfg.get("ssl") else smtplib.SMTP
    with smtp(cfg["host"]) as server:
        if cfg.get("auth"):
            server.login(*cfg["auth"])
        server.send_message(msg)
    print("mail has been sent!")


if __name__ == '__main__':
    main()
